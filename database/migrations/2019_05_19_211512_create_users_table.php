<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->bigInteger('rol_id')->unsigned()->default(2); // ID del rol asignado al usuario
            $table->foreign('rol_id')->references('id')->on('user_rols'); // Llave foranea
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert(
            array(
                [
                    'name'      => 'Administrador',
                    'email'     => 'admin@test.com',
                    'password'  => bcrypt('password'),
                    'rol_id'    => 1
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) { // Removemos la llave foranea
            $table->dropForeign('users_rol_id_foreign');
        });

        Schema::dropIfExists('users');
    }
}
