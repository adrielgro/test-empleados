<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('author'); // Autor
            $table->date('date'); // Fecha
            $table->longText('description'); // Descripción
            $table->bigInteger('user_id')->unsigned(); // ID del usuario
            $table->foreign('user_id')->references('id')->on('users'); // Llave foranea
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) { // Removemos la llave foranea
            $table->dropForeign('reports_user_id_foreign');
        });

        Schema::dropIfExists('reports');
    }
}
