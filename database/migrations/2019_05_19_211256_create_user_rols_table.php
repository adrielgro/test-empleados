<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rols', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name'); // Nombre del rol
            $table->string('rol'); // Identificador del rol
            $table->timestamps();
        });

        DB::table('user_rols')->insert(
            array(
                [
                    'name' => 'Administrador',
                    'rol' => 'administrator'
                ],
                [
                    'name' => 'Empleado',
                    'rol' => 'employee'
                ],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rols');
    }
}
