<?php

/*
|--------------------------------------------------------------------------
|                               AUTENTICACIÓN
|--------------------------------------------------------------------------
|*/
Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@register');
Route::post('logout', 'Auth\LoginController@logout');
Route::get('/', function () {
    return view('auth');
})->middleware('guest')->name('login');

/*
|--------------------------------------------------------------------------
|                                   PANEL
|--------------------------------------------------------------------------
|*/
Route::get('/home', function () {
    return view('panel');
})->middleware('auth')->name('home');

/*
|--------------------------------------------------------------------------
|                               CONTROLADORES
|--------------------------------------------------------------------------
|*/
Route::group(['middleware' => ['auth']], function () {

    Route::group(['middleware' => ['role:administrator']], function () {
        Route::get('rols', 'RolController@index');
        Route::get('users', 'UserController@index');
        Route::post('users/create', 'UserController@store');
        Route::post('users/update/{id}', 'UserController@update')->where('id', '[0-9]+');
        Route::post('users/delete/{id}', 'UserController@destroy')->where('id', '[0-9]+');
    });

    Route::get('reports', 'ReportController@index');
    Route::post('reports/create', 'ReportController@store');
    Route::post('reports/update/{id}', 'ReportController@update')->where('id', '[0-9]+');
    Route::post('reports/delete/{id}', 'ReportController@destroy')->where('id', '[0-9]+');

});
