<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'  => 'required|string|max:64',
            'email' => [
                'required','string','email','max:64',
                Rule::unique('users')->ignore(Auth::id()),
            ],
            'password'  => 'nullable|string|min:6',
            'rol'       => 'required|string|exists:user_rols,rol',
        ];
    }
}
