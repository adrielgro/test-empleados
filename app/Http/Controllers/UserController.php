<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\User;
use App\UserRol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::join('user_rols', 'users.rol_id', '=', 'user_rols.id')
            ->select('users.id', 'users.name as username', 'users.email', 'user_rols.name as rolname', 'user_rols.rol as rol')
            ->get();

        return response()->json($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $rol = UserRol::where('rol', $request->rol)->firstOrFail();

        try {
            $user = User::create([
                'name'      => $request->username,
                'email'     => $request->email,
                'password'  => Hash::make($request->password),
                'rol_id'    => $rol->id
            ]);
            $user->rolname = $rol->name;

            return response()->json($user, 200);
        } catch (\Exception $e) {
            return response()->json("Error", 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $rol = UserRol::where('rol', $request->rol)->firstOrFail();

        try {
            $user = User::find($id);
            $user->name     = $request->username;
            $user->email    = $request->email;
            $user->rol_id = $rol->id;

            if($request->password) $user->password = bcrypt($request->password);

            $user->save();

            return response()->json($rol->name, 200);
        } catch (\Exception $e) {
            return response()->json("Error", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json("Success", 200);
    }
}
