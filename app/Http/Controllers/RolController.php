<?php

namespace App\Http\Controllers;

use App\UserRol;
use Illuminate\Http\Request;

class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rols = UserRol::all();

        return response()->json($rols, 200);
    }
}
