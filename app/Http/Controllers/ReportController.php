<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportRequest;
use App\Report;
use App\User;
use App\UserRol;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::where('user_id', Auth::id())->get();

        return response()->json($reports, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportRequest $request)
    {
        try {
            $report = new Report;
            $report->author         = $request->author;
            $report->date           = $request->date;
            $report->description    = $request->description;
            $report->user_id        = Auth::id();
            $report->save();

            return response()->json($report->id, 200);
        } catch (\Exception $e) {
            return response()->json("Error", 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReportRequest $request, $id)
    {
        try {
            $report = Report::where(['id' => $id, 'user_id' => Auth::id()])->firstOrFail();
            $report->author         = $request->author;
            $report->date           = $request->date;
            $report->description    = $request->description;
            $report->save();

            return response()->json("Success", 200);
        } catch (\Exception $e) {
            return response()->json("Error", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report::where(['id' => $id, 'user_id' => Auth::id()])->firstOrFail();
        $report->delete();

        return response()->json("Success", 200);
    }
}
