const mix = require('laravel-mix');

mix.js([
    'resources/js/auth/app.js',
], 'public/js/auth.js')
    .styles([
        'resources/css/auth.css'
    ], 'public/css/auth.css');

mix.js([
    'resources/js/panel/app.js',
], 'public/js/panel.js')
    .styles([
        'resources/css/panel.css'
    ], 'public/css/panel.css');
