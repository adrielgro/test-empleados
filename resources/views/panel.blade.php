<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Panel</title>

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/panel.css') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
<div id="app">
    <v-app>
        <navbar-component :user_rol="{{ Auth::user()->rol_id }}"></navbar-component>
        <v-content>
            <v-container>
                <v-layout align-center justify-center row fill-height>
                    <v-flex xs12>
                        <transition name="fade" mode="out-in">
                            <router-view></router-view>
                        </transition>
                    </v-flex>
                </v-layout>
            </v-container>
        </v-content>
        <alert-snackbar></alert-snackbar>
    </v-app>
</div>

<script src="{{ asset('js/panel.js') }}"></script>
</body>
</html>
