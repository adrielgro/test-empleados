<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Autenticación</title>

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
<div id="app">
    <v-app>
        <v-content>
            <v-container fluid style="width: 80%; max-width: 1000px; height: calc(100vh - 0px); min-height: 500px">
                <v-layout align-center justify-center row fill-height>
                    <v-flex xs12>
                        <v-card flat style="border-radius: 8px">
                            <transition name="fade" mode="out-in">
                                <router-view></router-view>
                            </transition>
                        </v-card>
                    </v-flex>
                </v-layout>
            </v-container>
        </v-content>
        <alert-snackbar></alert-snackbar>
    </v-app>
</div>

<script src="{{ asset('js/auth.js') }}"></script>
</body>
</html>
