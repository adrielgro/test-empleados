import Vue      from 'vue'
import router   from './router'
import Vuetify  from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

window.EventBus = new Vue();
Vue.use(Vuetify);

Vue.component('alert-snackbar', require('../components/AlertsComponent').default);
Vue.component('navbar-component', require('./components/NavbarComponent').default);

new Vue({
    el: '#app',
    router
});
