import Vue              from 'vue'
import Router           from 'vue-router'

import Home     from '../views/Home'
import Users    from '../views/User'
import Reports  from '../views/Report'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/users',
            component: Users
        },
        {
            path: '/reports',
            component: Reports
        }
    ]
});
