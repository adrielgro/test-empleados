import Vue      from 'vue'
import Router   from 'vue-router'

// Vistas
import Login    from '../views/Login'
import Register from '../views/Register'


Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            component: Login
        },
        {
            path: '/register',
            component: Register
        },
    ]
});
